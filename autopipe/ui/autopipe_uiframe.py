# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'autopipe/ui/autopipe.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Autopipe(object):
    def setupUi(self, Autopipe):
        Autopipe.setObjectName("Autopipe")
        Autopipe.resize(1109, 899)
        self.centralwidget = QtWidgets.QWidget(Autopipe)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.load_video_button = QtWidgets.QPushButton(self.centralwidget)
        self.load_video_button.setObjectName("load_video_button")
        self.horizontalLayout_7.addWidget(self.load_video_button)
        self.classify_defect_button = QtWidgets.QPushButton(self.centralwidget)
        self.classify_defect_button.setObjectName("classify_defect_button")
        self.horizontalLayout_7.addWidget(self.classify_defect_button)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.start_camera_button = QtWidgets.QPushButton(self.centralwidget)
        self.start_camera_button.setObjectName("start_camera_button")
        self.horizontalLayout.addWidget(self.start_camera_button)
        self.stop_video_button = QtWidgets.QPushButton(self.centralwidget)
        self.stop_video_button.setObjectName("stop_video_button")
        self.horizontalLayout.addWidget(self.stop_video_button)
        self.horizontalLayout_7.addLayout(self.horizontalLayout)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        self.verticalLayout_11 = QtWidgets.QVBoxLayout()
        self.verticalLayout_11.setObjectName("verticalLayout_11")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.defect_1 = QtWidgets.QLabel(self.centralwidget)
        self.defect_1.setObjectName("defect_1")
        self.horizontalLayout_10.addWidget(self.defect_1)
        self.defect_2 = QtWidgets.QLabel(self.centralwidget)
        self.defect_2.setObjectName("defect_2")
        self.horizontalLayout_10.addWidget(self.defect_2)
        self.defect_3 = QtWidgets.QLabel(self.centralwidget)
        self.defect_3.setObjectName("defect_3")
        self.horizontalLayout_10.addWidget(self.defect_3)
        self.defect_4 = QtWidgets.QLabel(self.centralwidget)
        self.defect_4.setObjectName("defect_4")
        self.horizontalLayout_10.addWidget(self.defect_4)
        self.defect_5 = QtWidgets.QLabel(self.centralwidget)
        self.defect_5.setObjectName("defect_5")
        self.horizontalLayout_10.addWidget(self.defect_5)
        self.verticalLayout_11.addLayout(self.horizontalLayout_10)
        self.verticalLayout.addLayout(self.verticalLayout_11)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.video = QtWidgets.QLabel(self.centralwidget)
        self.video.setText("")
        self.video.setObjectName("video")
        self.horizontalLayout_9.addWidget(self.video)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.processed_img_label = QtWidgets.QLabel(self.centralwidget)
        self.processed_img_label.setText("")
        self.processed_img_label.setObjectName("processed_img_label")
        self.horizontalLayout_11.addWidget(self.processed_img_label)
        self.horizontalLayout_9.addLayout(self.horizontalLayout_11)
        self.verticalLayout.addLayout(self.horizontalLayout_9)
        self.verticalLayout_5.addLayout(self.verticalLayout)
        Autopipe.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Autopipe)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1109, 21))
        self.menubar.setObjectName("menubar")
        Autopipe.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Autopipe)
        self.statusbar.setObjectName("statusbar")
        Autopipe.setStatusBar(self.statusbar)

        self.retranslateUi(Autopipe)
        QtCore.QMetaObject.connectSlotsByName(Autopipe)

    def retranslateUi(self, Autopipe):
        _translate = QtCore.QCoreApplication.translate
        Autopipe.setWindowTitle(_translate("Autopipe", "Autopipe"))
        self.load_video_button.setText(_translate("Autopipe", "Load Video"))
        self.classify_defect_button.setText(_translate("Autopipe", "Capture Defect"))
        self.start_camera_button.setText(_translate("Autopipe", "Start Camera"))
        self.stop_video_button.setText(_translate("Autopipe", "Stop Video"))
        self.defect_1.setText(_translate("Autopipe", "img1"))
        self.defect_2.setText(_translate("Autopipe", "img2"))
        self.defect_3.setText(_translate("Autopipe", "img3"))
        self.defect_4.setText(_translate("Autopipe", "img4"))
        self.defect_5.setText(_translate("Autopipe", "img5"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Autopipe = QtWidgets.QMainWindow()
    ui = Ui_Autopipe()
    ui.setupUi(Autopipe)
    Autopipe.show()
    sys.exit(app.exec_())

