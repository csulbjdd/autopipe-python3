import cv2
from datetime import datetime
import sys
import winsound

from PyQt5.QtCore import QThread, pyqtSignal, Qt, QTimer
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QMainWindow, QFileDialog, QMessageBox
from PyQt5 import QtWidgets

from autopipe.ui.autopipe_uiframe import Ui_Autopipe
from autopipe.predictions import process_image
from autopipe import frames
from autopipe import image_process as ip


class PlayDefectSoundThread(QThread):
    def __init__(self, sound):
        super().__init__()
        self.sound = sound

    def run(self):
        winsound.PlaySound(self.sound, winsound.SND_FILENAME)


class AutopipeGui(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Autopipe()
        self.ui.setupUi(self)
        self.init_buttons()
        self.init_classify_defect()
        self.pixmaps = [self.ui.defect_1, self.ui.defect_2,
                              self.ui.defect_3, self.ui.defect_4,
                              self.ui.defect_5]
        self.sound_thread = PlayDefectSoundThread('data/sounds/defect.wav')
        self.sound = True
        self.first_defect_seen = True

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_F5 or event.key() == Qt.Key_Escape:
            self.mute_sounds()

    def mute_sounds(self):
        self.sound = True if not self.sound else False

    def init_buttons(self):
        self.ui.load_video_button.clicked.connect(self.load_video)
        self.ui.start_camera_button.clicked.connect(self.load_camera)
        self.ui.stop_video_button.clicked.connect(self.stop_video_stream)
        self.ui.classify_defect_button.clicked.connect(self.capture_defect)

    def init_classify_defect(self):
        self.defectType = QMessageBox()
        self.defectType.setIcon(QMessageBox.Information)
        self.defectType.setText("Type of defect?")

        clButton = QtWidgets.QPushButton('CL')
        clButton.clicked.connect(lambda: self.reclassify('CL'))
        flButton = QtWidgets.QPushButton('FL')
        flButton.clicked.connect(lambda: self.reclassify('FL'))
        bButton = QtWidgets.QPushButton('B')
        bButton.clicked.connect(lambda: self.reclassify('B'))
        ccButton = QtWidgets.QPushButton('CC')
        ccButton.clicked.connect(lambda: self.reclassify('CC'))
        rfjButton = QtWidgets.QPushButton('RFJ')
        rfjButton.clicked.connect(lambda: self.reclassify('RFJ'))
        cmButton = QtWidgets.QPushButton('CM')
        cmButton.clicked.connect(lambda: self.reclassify('CM'))
        fmButton = QtWidgets.QPushButton('FM')
        fmButton.clicked.connect(lambda: self.reclassify('FM'))
        fcButton = QtWidgets.QPushButton('FC')
        fcButton.clicked.connect(lambda: self.reclassify('FC'))
        fsButton = QtWidgets.QPushButton('FS')
        fsButton.clicked.connect(lambda: self.reclassify('FS'))
        csButton = QtWidgets.QPushButton('CS')
        csButton.clicked.connect(lambda: self.reclassify('CS'))
        otherButton = QtWidgets.QPushButton('OTHER')
        otherButton.clicked.connect(lambda: self.reclassify('OTHER'))

        self.defectType.addButton(clButton, QMessageBox.ActionRole)
        self.defectType.addButton(flButton, QMessageBox.ActionRole)
        self.defectType.addButton(bButton, QMessageBox.ActionRole)
        self.defectType.addButton(ccButton, QMessageBox.ActionRole)
        self.defectType.addButton(rfjButton, QMessageBox.ActionRole)
        self.defectType.addButton(cmButton, QMessageBox.ActionRole)
        self.defectType.addButton(fmButton, QMessageBox.ActionRole)
        self.defectType.addButton(fcButton, QMessageBox.ActionRole)
        self.defectType.addButton(fsButton, QMessageBox.ActionRole)
        self.defectType.addButton(csButton, QMessageBox.ActionRole)
        self.defectType.addButton(otherButton, QMessageBox.ActionRole)

    def reclassify(self, name):
        ret, frame = self.capture.read()
        img_time = str(datetime.now())
        img_time = img_time.replace(":", "-") # Window file names cannot contain ':'
        image_name = "data\\new_images\\" + name + img_time + ".png"
        cv2.imwrite(image_name, frame)

    def load_video(self):
        """Allows the user to pick a video file to load into the video player"""
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "Load file",
                                                   "", "All Files (*)",
                                                   options=options)
        if file_name:
            print(file_name)
            self.video_source = file_name
            self.start_video_stream()

    def load_camera(self):
        self.video_source = 0
        self.start_video_stream()

    def start_video_stream(self):
        self.capture = cv2.VideoCapture(self.video_source)
        self.timer = QTimer()
        self.timer.timeout.connect(self.display_video_stream)
        self.timer.start(30)

    def stop_video_stream(self):
        self.capture.release()
        self.timer.stop()

    def display_video_stream(self):
        ret, frame = self.capture.read()
        res, class_type, previous_defects = process_image(frame)
        res = cv2.resize(res, (640, 480))

        # Keep track of defects for queue usage
        if class_type == frames.Pipe.DEFECT:
            place_defects_into_pixmaps(self.pixmaps, previous_defects)

            if self.sound and self.first_defect_seen :
                self.sound_thread.start()
                self.first_defect_seen = False
        else:
            self.first_defect_seen = True


        pixmap = convert_from_frame_to_QPixmap(res)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        kernel_size = 7
        blur = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)
        canny = cv2.Canny(blur, 50, 150)
        canny = ip.crop_image(canny, gray=True)
        canny = cv2.resize(canny, (640, 480))

        canny_pixmap = convert_from_frame_to_QPixmap(canny, gray=True)
        self.ui.processed_img_label.setPixmap(canny_pixmap)
        self.ui.video.setPixmap(pixmap)

    def set_frame(self, frame):
        pixmap = convert_from_frame_to_QPixmap(frame)
        self.ui.video.setPixmap(pixmap)

    def capture_defect(self):
        self.defectType.exec_()


def convert_from_frame_to_QPixmap(frame, gray=False, size=(640, 480)):
    if gray:
        qimage_format = QImage.Format_Indexed8
    else:
        qimage_format = QImage.Format_RGB888
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    convertToQtFormat = QImage(frame.data, frame.shape[1], frame.shape[0], qimage_format)
    convertToQtFormat = QPixmap.fromImage(convertToQtFormat)
    p = convertToQtFormat.scaled(*size, Qt.KeepAspectRatio)
    return p


def place_defects_into_pixmaps(pixmaps, defects):
    defects = [convert_from_frame_to_QPixmap(defect, size=(256, 256))
               for defect in defects]
    for pixmap, defect_frame in zip(pixmaps, defects):
        pixmap.setPixmap(defect_frame)


def main():
    app = QApplication(sys.argv)
    window = AutopipeGui()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
